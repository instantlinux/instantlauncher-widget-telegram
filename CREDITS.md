# Credits
Here I will credit all images that I used in this widget.
## Icons
**Conversation** *(this logo when you don't have selected a chat)* icon made by [Good Ware](https://www.flaticon.com/authors/good-ware) from flaticon.com, download [here](https://www.flaticon.com/free-icon/chat_784683).