# InstantLauncher widget - Telegram

> Widget for [InstantLauncher](https://github.com/InstantCodee/instantLauncher)

## What does it do?

> This widget display's your telegram contacts, receive and send messages.

## How to install?
1. Create the widgets folder with `sudo mkdir -p /usr/share/instantdesktop/instantlauncher/widgets`
2. Clone this repository `git clone https://bitbucket.org/instantlinux/instantlauncher-widget-telegram.git`
3. Go inside the folder with `cd instantlauncher-widget-telegram`
4. Run `npm install` to install all dependencies. (This may take a little while)
5. Run `npm run build` to compile TypeScript to JavaScript.
6. Run `npm test` to copy all the content to your widget folder.

Now you're ready to use this widget. Make sure you have compiled instantLauncher (follow the instructions there) and restarted it.

**NOTE:** You need your own API Key and Hash for now to use this widget. Click [here](https://my.telegram.org) to get one. Open `src/config.ts` and put your API Key and Hash into the entry's.

## Meta
Distributed under the GPL-3.0 license. See ``LICENSE`` for more information.

## Contributing

1. Fork it (<https://github.com/InstantCodee/instantlauncher-widget-telegram.git>)
1. Create your feature branch (`git checkout -b feature/fooBar`)
1. Commit your changes (`git commit -am 'Add some fooBar'`)
1. Push to the branch (`git push origin feature/fooBar`)
1. Create a new Pull Request
