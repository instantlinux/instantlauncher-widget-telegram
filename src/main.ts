/**
 * Telegram widget for instantLauncher by @Mondei1 (instantLinux Team).
 */
import * as fs from 'fs';                   // Built-in library to access files on disk.
import * as path from 'path';               // Built-in library to get path of a file.
import { MTProto } from 'telegram-mtproto'; // Official Telegram API. (https://github.com/zerobias/telegram-mtproto)
import { CONFIG } from './config';          // Config file in this folder.
import * as storage from './storage';       // To store your Telegram session.
import { Overview } from './overview';      // Contact overview.

// Prefix - used on debug messages. (press CTRL + SHIFT + i in instantLauncher, then activate log level "verbose" under console)
export const PREFIX = "[TelegramWidget] ";

// Current state.
enum TelegramState {
    ACTIVATION, // Not logged in yet.
    PENDING,    // User have to enter confirmation code.
    LOGGED_IN   // User is logged in.
}

// Declare phone number with code.
const phone = {
    num : '',    // +9996620001
    code: ''     // 22222
}

let user: any = null;
let client: any = null;
let currentState: TelegramState;
let phoneCodeHash: any = null;

// API settings. api_id is hidden because we are not allowed to show him. 
const api: any = {
    layer          : 57,
    initConnection : 0x69796de9,
    api_id         : CONFIG.api_key
}

// This is currently in development, so "dev" is set to true.
const server: any = {
    dev: false
}

// Define storage and debug.
export const app = {
    storage: storage
}

// This function return's a HTMLElement to display the widget. (The "Main" function)
export default function createWidget(widgetfolder: string): Promise<HTMLElement> {
    client = MTProto({ server, api, app }) // Init new instance of MTProto.
    return new Promise<HTMLElement>(async (resolve, reject) => {
        currentState = TelegramState.ACTIVATION;

        // Auto sign in.
        console.log("Result: " + await app.storage.get('signedin'))
        if (!(await app.storage.get('signedin'))) {
            console.log('not signed in')
        } else {
            currentState = TelegramState.LOGGED_IN;
            console.log('already signed in')
            resolve(await Overview(widgetfolder, client));
        }

        /*
         * If we arn't logged in, continue with that:
         */

        // Read html file.
        let html: string = fs.readFileSync(path.join(widgetfolder, "src/html/login.html")).toString();
        html = html.replace(/%widgetFolder%/gi, widgetfolder);

        // Create HTMLElement
        let telegramContainer = document.createElement("div");
        telegramContainer.className = "telegram";
        telegramContainer.innerHTML = html.toString();

        // Add phone number input
        let input_phoneCode = document.createElement("input");
        input_phoneCode.placeholder = "+49";

        // Add button to press 'Continue' and 'Finish'
        let button_continue = document.createElement("button");
        button_continue.className = "continue";

        button_continue.textContent = "Continue";

        /* This will execute sendCode() when the "Continue" button get clicked.. */
        button_continue.onclick = async () => {
            // Check If input is invaild.
            if(input_phoneCode.value == "") {
                const prev_placeholder = input_phoneCode.placeholder;
                input_phoneCode.style.borderBottom = "1px solid rgba(236, 5, 5, 1)"
                input_phoneCode.placeholder = "This field cannot be empty!"
                setTimeout(() => {
                    input_phoneCode.placeholder = prev_placeholder;
                    input_phoneCode.style.borderBottom = "1px solid rgba(217, 217, 217, 0.15)";
                }, 1000);
                return;
            }
            if(currentState == TelegramState.ACTIVATION) {
                phone.num = input_phoneCode.value;
                sendCode();
        
                // Switch to 'PENDING'.
                currentState = TelegramState.PENDING;
                input_phoneCode.value = "";
                input_phoneCode.placeholder = "Enter your code that you received via Telegram."
                button_continue.textContent = "Finish"
            } else if(currentState == TelegramState.PENDING) {
                phone.code = input_phoneCode.value;
                signIn(telegramContainer, widgetfolder);
            }        
        }

        telegramContainer.appendChild(input_phoneCode);     // Add phone number input to main container.
        telegramContainer.appendChild(button_continue);     // Add continue button to main container.

        // Return final HTMLElement, so instantLauncher can display it.
        resolve(telegramContainer);
    });
}

/** ============= [ FUNCTIONS ] ============= */

// Request code. User will receive a message via SMS.
function sendCode() {  
    async function connect() {
        const { phone_code_hash } = await client('auth.sendCode', {
            phone_number  : phone.num,
            sms_type      : 0,  // Mode to receive message via Telegram and not SMS. (who uses SMS today? Pff...)
            api_id        : CONFIG.api_key,
            api_hash      : CONFIG.api_hash,
            lang_code     : "de"
        })
        console.log(JSON.stringify(phone_code_hash))
        phoneCodeHash = phone_code_hash;
        return 0;
    }
    connect();
}

// Get access to Telegram account.
async function signIn(div: HTMLDivElement, widgetfolder: string) {
    user = await client('auth.signIn', {
        phone_number   : phone.num,
        phone_code_hash: phoneCodeHash,
        phone_code     : phone.code
    })
    app.storage.set("signedin", true)
    div.innerHTML = (await Overview(widgetfolder, client)).innerHTML    // Load overview
    return 0;
}

// Simple function to convert timestamp into "DD.MM.YYYY" format. Used to display last seen.
export function timestampToDate(timestamp: number) {
    const date = new Date(timestamp*1000);
    if(isNaN(date.getDay())) return ""; // If this is the case, it's the user itself.
    return date.getDay() + "." + date.getMonth() + "." + date.getFullYear()
}