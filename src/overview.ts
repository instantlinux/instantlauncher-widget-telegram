/**
 * This is actually the biggest part of all because this is the overview of your
 * contacts and the place where you send messages.
 */
import * as fs from 'fs';                   // Built-in library to access files on disk.
import * as path from 'path';               // Built-in library to get path of a file.
import { MTProto } from 'telegram-mtproto'; // Official Telegram API. (https://github.com/zerobias/telegram-mtproto)
import { CONFIG } from './config';          // Config file in this folder.
import * as storage from './storage';       // To store your Telegram session.
import { PREFIX, timestampToDate, app } from './main';

export async function Overview(widgetfolder: string, client: any): Promise<HTMLElement> {
    let CURRENT_CHAT = 0;   // User ID of contact.
    return new Promise<HTMLElement>(async (resolve, reject) => {
        // Read html file.
        let html: string = fs.readFileSync(path.join(widgetfolder, "src/html/overview.html")).toString();
        html = html.replace(/%widgetFolder%/gi, widgetfolder);

        // Create HTMLElement
        let telegramContainer: HTMLElement = document.createElement("div");
        telegramContainer.className = "telegram";
        telegramContainer.innerHTML = html.toString();

        // Fetch all contacts.
        const contactsList: Array<any> = (await client("contacts.getContacts")).users;
        console.log(contactsList)

        // Create ul-Object to list all contacts.
        let ul_contacts = document.createElement("ul");
        
        // Fill ul_contacts with contacts.
        for(let i = 0; i < contactsList.length; i++) {
            const contact = contactsList[i];
            console.debug(PREFIX, "Aktueller Kontakt: ", contact)
            // Get full information about contact.
            const fullContact: any = await client("users.getFullUser", {
                id: {
                    _: "inputUser",
                    user_id: contact.id
                }
            })
            console.debug(fullContact)

            // Get profile image.
            /*console.debug("Volume: ", fullContact)
            const image = await client("upload.getFile", {
                location: {
                    _: 'inputFileLocation',
                    volume_id: <number>(fullContact.user.photo.photo_small.volume_id),
                    local_id: <number>(fullContact.user.photo.photo_small.local_id),
                    secret: <number>(fullContact.user.photo.photo_small.secret)
                },
                offset: 0,
                limit: 1,
                dcID: 4, fileDownload: true, createNetworker: true
            })
            console.debug(image)*/
            let li = document.createElement("li");          // Create li object.
            let contactName = document.createElement("p");  // Create p object for contact name.
            let avatar = document.createElement("img");
            let lastSeen = document.createElement("small");

            contactName.textContent = contact.username;
            lastSeen.textContent = "Last seen: " + timestampToDate(contact.status.was_online);

            li.appendChild(contactName)
            li.appendChild(lastSeen)

            // Set onclick listener to set current chat on click.
            li.onclick = function() {
                // TODO Insert something
            }
            
            // Only add contect If name isn't empty.
            if(contactName.textContent != "") ul_contacts.appendChild(li);
        }

        // Display conversation.
        let div_conversation_box = document.createElement("div");       // Is the grey background
        div_conversation_box.className = "select_conversation_box"

        let div_conversation_content = document.createElement("div");   // Is the image and text
        div_conversation_content.className = "select_converstation_content";

        let h3_conversation_nonSelect = document.createElement("h3")
        h3_conversation_nonSelect.textContent = "Please select a chat to display.";

        let img_conversation_nonSelect = document.createElement("img"); // Icon made by https://www.flaticon.com/authors/good-ware from flaticon.com
        img_conversation_nonSelect.src = "%widgetfolder%/src/images/Conversation.png";

        div_conversation_content.appendChild(img_conversation_nonSelect);
        div_conversation_content.appendChild(h3_conversation_nonSelect);
        div_conversation_box.appendChild(div_conversation_content)

        // Send message
        console.debug(PREFIX, "Sending message to myself ...");
        const message = await client("messages.sendMessage", {
            peer: {
                _: 'inputPeerSelf'
            },
            message: "Such wow! This message is sent by TelegramWidget. Currently in developing phase.",
            random_id: Math.round(Math.random() * (Number.MAX_VALUE - 0) + 0)
        })
        console.debug(PREFIX, message)

        // Get dialog.
        const dialog = await client("messages.getDialogs", {
            offset: 1,
            max_id: 1000,
            limit: 50
        })
        console.debug(dialog);

        telegramContainer.appendChild(ul_contacts);
        telegramContainer.appendChild(div_conversation_box);

        // Replace %widgetfolder% with real path.
        telegramContainer.innerHTML = telegramContainer.innerHTML.replace(/%widgetfolder%/gi, widgetfolder);

        resolve(telegramContainer);
    })
}