/**
 * Custom storage to save Telegram session.
 * Binding localForage to AsyncStorage so that telegram-mtproto can sava active session.
 */
import { AsyncStorage } from "telegram-mtproto";
import localForage from "localforage";
import { PREFIX } from "./main";

export function setup() {
    localForage.config({
        driver: localForage.LOCALSTORAGE,
        name: "telegramWidget",
        description: "Saves active Telegram session.",
        storeName: "telegram_session"
    })
}

export function get(key: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
        const ret = await localForage.getItem(key);
        console.debug(PREFIX, "Get value, ", key, "return", ret)
        resolve(ret)
    })
}

export function set(key: string, value: any): Promise<void> {
    console.debug(PREFIX, "Set ", key, " in local storage to ", value)
    return new Promise(async (resolve, reject) => {
        await localForage.setItem(key, value);
        resolve();
    })
}

export function remove(...keys: string[]): Promise<void> {
    console.debug(PREFIX, "Removed ", keys, " from local storage")
    return new Promise(async (resolve, reject) => {
        for(let i = 0; i < keys.length; i++) {
            await localForage.removeItem(keys[i]);
        }
        resolve();
    })
}

export function clear(): Promise<any> {
    console.debug(PREFIX, "Cleared local storage")
    return new Promise(async (resolve, reject) => {
        await localForage.clear();
        resolve();
    })
}