export interface _CONFIG {
    api_key: number,
    api_hash: string
}

/* This is the config, ignore this above. Default values are the official key/hash for tests purpose only, take a look here https://github.com/zerobias/telegram-mtproto#usage */
export const CONFIG: _CONFIG = {
    api_key: 49631,
    api_hash: "fb050b8f6771e15bfda5df2409931569"
}